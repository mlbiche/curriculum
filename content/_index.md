+++
statutProfessionnel = 'Développeur Web indépendant'
abstract = "Développeur indépendant depuis 2022, je développe des applications Web pour des projets à impact positif. Ces derniers mois, au sein de [beta.gouv.fr](https://beta.gouv.fr/) et de l'[Incubateur des Territoires](https://incubateur.anct.gouv.fr/), je participe au développement de Mon Suivi Social, un outil simple et intuitif pour faciliter le suivi des bénéficiaires des structures d'accompagnement social."
niveauAnglais = "Je parle anglais couramment à l'écrit comme à l'oral (IELTS 7.0 - 2018)."
technologies = ["VanillaES6", "Vue", "Svelte", "Node.js", "TypeScript", "CSS / SCSS", "Starlette / FastAPI (Python)"]
appetences = [
  "La conception minimaliste, incrémentale et orientée utilisateur",
"Le numérique [acceptable (title: Lire l&apos;article &quot;Numérique responsable, critique d&apos;un oxymore&quot; qui propose le terme de &quot;numérique acceptable&quot;)](https://louisderrac.com/2023/06/numerique-responsable-critique-dun-oxymore/), inclusif, accessible et fonctionnel",
"Les technologies minimalistes",
"Les projets à caractère éthique et durable",
"Le code source ouvert"
]

[addresse]
  courriel = 'thibaud.malbert@pm.me'
  linkedin = 'https://www.linkedin.com/in/thibaud-malbert'
  gitlab = 'https://gitlab.com/mlbiche'
  github = 'https://github.com/mlbiche'

[[experiences]]
  structure = "beta.gouv.fr, Incubateur des Territoires"
  intitule = "Développeur Web"
  dateDebut = "2022-03-01"
  anneeDebut = "2022"
  lieu = "Télétravail"
  technologies = "Nuxt 3/Vue.js 3, Prisma, Directus, PostgreSQL"
  description = "J'interviens dans la mise en place technique de la startup d'État [Mon Suivi Social (title: Accéder au code source du projet Mon Suivi Social)](https://gitlab.com/incubateur-territoires/startups/monsuivisocial/monsuivisocial-v2), un outil simple et intuitif pour faciliter le suivi des bénéficiaires des structures d'accompagnement social."


[[experiences]]
  structure = "Millionroads"
  intitule = "Développeur front-end"
  anneeDebut = "2020"
  duree = "1 an, 4 mois"
  lieu = "Avignon / Télétravail"
  technologies = "Angular 13"
  description = "J'ai été impliqué dans le développement de [My Road](https://myroad.letudiant.fr/), une carte d'orientation en accès libre à destination des futurs étudiants."


[[experiences]]
  structure = "Scopyleft"
  intitule = "Stage développeur Web"
  anneeDebut = "2020"
  duree = "5 mois"
  lieu = "Télétravail"
  technologies = "Svelte 3, Starlette"
  description = "J'ai construit en autonomie [Aposto (title: Accéder au code source du projet Aposto)](https://github.com/etceterra/aposto-app/), un logiciel de facturation pour thérapeutes complémentaires suisses, et son API, dans une démarche incrémentale, minimaliste et libre."


[[experiences]]
  structure = "GEEV"
  intitule = "Stage développeur Web"
  anneeDebut = "2018"
  duree = "3 mois"
  lieu = "Bordeaux"
  technologies = "Angular 6, hapi.js, MongoDB"
  description = "J'ai participé au développement de l'[application Web de GEEV](https://www.geev.com/), la première application mobile pour donner ou récupérer des objets et de la nourriture entre particuliers."


[[formations]]
  structure = "UTS"
  intitule = "Semestre à l'étranger"
  anneeDebut = "2019"
  duree = "4 mois"
  iconePays = "🇦🇺"
  lieu = "Sydney, Australie"
  technologies = "React.js, express.js, MongoDB"
  description = "Développement d'applications Web, cyber-sécurité, analyse de données et UI/UX."


[[formations]]
  structure = "ENSEIRB-MATMECA"
  intitule = "Diplôme d'ingénieur informatique"
  anneeDebut = "2016"
  anneeFin = "2020"
  lieu = "Bordeaux"


[[formations]]
  structure = "La Prépa des INP"
  intitule = "Classe préparatoire scientifique"
  anneeDebut = "2014"
  anneeFin = "2016"
  lieu = "Grenoble"


[[cesures]]
  structure = "400 Lieues sur la Terre"
  anneeDebut = "2018"
  duree = "6 mois"
  iconePays = "🇳🇵"
  lieu = "Népal"
  description = "Avec 3 amis, j'ai réalisé [un projet de 6 mois (title: Voir le site du projet 400 Lieues sur la Terre)](https://400lieuessurlaterre.wordpress.com/) au Népal composé de 1000km de marche sur le *Great Himalayan Trails* et d'une mission de bénévolat dans une école. J'ai mis en place des ateliers d’informatique pour les élèves et j'ai développé le [nouveau site Web (title: Accéder au code source du site de l'école VHMaVi)](https://github.com/mlbiche/vhmavi-website)."
+++
