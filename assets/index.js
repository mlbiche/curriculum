dayjs.locale('fr')

/**
 * @type {import('dayjs').Dayjs}
 */
const dateDuJour = dayjs()

const elementsAvecDateDebut = /** @type {NodeListOf<HTMLSpanElement>} */ (
  document.querySelectorAll('[data-date-debut]')
)

elementsAvecDateDebut.forEach(elementAvecDateDebut => {
  const dateDebut = obtientDateDebut(elementAvecDateDebut)
  if (!dateDebut) {
    return
  }

  const { annees, mois } = obtientDifferenceDepuisAujourdhui(dateDebut)
  const libelleDuree = obtientLibelleDuree(annees, mois)
  if (libelleDuree) {
    elementAvecDateDebut.innerText = libelleDuree
  }
})

/**
 * @param {HTMLSpanElement} elementAvecDateDebut
 * @returns {import('dayjs').Dayjs | null}
 */
function obtientDateDebut(elementAvecDateDebut) {
  const attributDateDebut = elementAvecDateDebut.dataset.dateDebut
  if (!attributDateDebut) {
    return null
  }

  return dayjs(attributDateDebut)
}

/**
 * @param {import('dayjs').Dayjs} dateDebut
 */
function obtientDifferenceDepuisAujourdhui(dateDebut) {
  const annees = dateDuJour.diff(dateDebut, 'year')
  const mois = dateDuJour.diff(dateDebut, 'month') - annees * 12
  return { annees, mois }
}

/**
 * @param {number} annees
 * @param {number} mois
 */
function obtientLibelleDuree(annees, mois) {
  let libelleDuree = ' - '
  if (annees) {
    libelleDuree += `${annees} an`
    if (annees > 1) {
      libelleDuree += 's'
    }
  }

  if (mois) {
    if (annees) {
      libelleDuree += ', '
    }
    libelleDuree += `${mois} mois`
  }
  return libelleDuree === ' - ' ? '' : libelleDuree
}
